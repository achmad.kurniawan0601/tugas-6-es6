   // Soal 1
   let luas = (panjang, lebar) => panjang * lebar;
   let keliling = (panjang, lebar) => 2 * (panjang + lebar);

   // soal 2
   const newFunction = (firstName, lastName) => {
       return{
           firstName,
           lastName,
           fullName: function(){
               console.log(firstName + " " + lastName);
           }
       }
   }

   // Driver code
   newFunction("William", "Imoh").fullName();

   // soal 3
   const newObject = {
       firstName: "Muhammad",
       lastName: "Iqbal Mubarok",
       address: "Jalan Ranamanyar",
       hobby: "playing football"
   };

   const {firstName, lastName, address, hobby} = newObject;

   // Driver code
   console.log(firstName, lastName, address, hobby);

   // Soal 4
   const west = ["Will", "Chris", "Sam", "Holly"];
   const east = ["Gill", "Brian", "Noel", "Maggie"];
   const combine = [...west, ...east];
   console.log(combine);

   // Soal 5
   const planet = "earth";
   const view = "glass";

   var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet;
   let after  = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`;

   console.log("Before: " + before+'\n');
   console.log("After: " + after);